const request = require('supertest');
const app = require('./src/app');

describe('POST /typing', () => {
  test('should return 201', async () => {
    const response = await request(app).post('/typing')
      .send({ speed: 200, races: 900 });
    expect(response.statusCode).toBe(201);
  });
  test('should return 400', async () => {
    const response = await request(app).post('/typing');
    expect(response.statusCode).toBe(400);
  });
});
