const express = require('express');
const bodyParser = require('body-parser');
const csvAppend = require('./csv');

const app = express();
const JSONParse = bodyParser.json({ type: 'application/json' });
app.post('/typing', JSONParse, (req, res) => {
  if (typeof (req.body.speed) === 'number' && typeof (req.body.races) === 'number') {
    const entries = [];
    const log = [
      req.body.speed,
      req.body.races,
      new Date(),
    ];
    entries.push(log);
    csvAppend.append(entries);
    res.sendStatus(201);
  } else {
    res.sendStatus(400);
  }
});

module.exports = app;
