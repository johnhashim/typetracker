require('dotenv').config();
const csvAppend = require('./csv');
const start = require('./server');

const args = process.argv.slice(2);
async function typeTracker() {
  if (args.includes('log') && args.includes('--speed') && args.includes('--races')) {
    const entries = [];
    const log = [
      args[args.indexOf('--speed') + 1],
      args[args.indexOf('--races') + 1],
      new Date(),
    ];
    entries.push(log);
    await csvAppend.append(entries);
  }
}
function startServer() {
  if (args.includes('server')) {
    start();
  }
}
function main() {
  switch (process.argv[2]) {
    case 'log':
      typeTracker();
      break;
    case 'server':
      startServer();
      break;
    default:
      console.log('Unknown command please try `node typetracker.js log ....` or`node typetracker.js server`');
      break;
  }
}
main();
