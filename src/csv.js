const { writeToStream } = require('@fast-csv/format');
const fs = require('fs');

async function append(rows) {
  const stream = fs.createWriteStream(process.env.CSV, { flags: 'a' });
  await stream.write('\n');
  await writeToStream(stream, rows);
}

module.exports = { append };
